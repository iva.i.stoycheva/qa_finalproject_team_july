package pages.myPage;

import base.AlreadyLoggedBaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.MyPage;

public class MyPageTest extends AlreadyLoggedBaseTest {
    @Test(priority = 2)
    public void checkMyPage() throws InterruptedException {
        sleepForNMs(1000);
        MyPage checkMyPage = homePage.clickMyPage();
        String MyPagTitle = checkMyPage.getTitle();

        Assert.assertEquals(MyPagTitle,"User Page");
        assertElementPresent("xpath.MyPageBody");
        assertElementPresent("xpath.myPageSection");

    }
}
