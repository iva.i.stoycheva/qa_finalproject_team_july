package pages;

import org.openqa.selenium.WebDriver;

public class KidsEntertainmentRequest extends BasePage {
    public KidsEntertainmentRequest(WebDriver driver) {
        super(driver);
    }

    public String getTitle(){
        return this.driver.getTitle();
    }
}
