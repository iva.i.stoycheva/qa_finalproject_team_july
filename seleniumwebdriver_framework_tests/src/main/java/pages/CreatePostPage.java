package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.Utils;

public class CreatePostPage extends BasePage {

    public CreatePostPage(WebDriver driver) {
        super(driver);
    }

    public void setPostTitle(String postTitle){
        WebDriverWait wait2 = new WebDriverWait(driver, 10);
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.postTitleField"));
        driver.findElement(By.xpath(Utils.getUIMappingByKey("xpath.postTitleField"))).sendKeys(postTitle);
    }

    public void createPostContent(String content){
        WebDriverWait wait2 = new WebDriverWait(driver, 10);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey("xpath.postContent"))));
        driver.findElement(By.xpath(Utils.getUIMappingByKey("xpath.postContent"))).sendKeys(content);
    }

    public UserPostsListPage submitPost(){
        driver.findElement(By.xpath(Utils.getUIMappingByKey("xpath.submitPostButton"))).click();
        return new UserPostsListPage(driver);
    }
}
