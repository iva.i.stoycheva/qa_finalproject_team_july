package base;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.NewsfeedPage;
import util.Utils;

import java.util.concurrent.TimeUnit;

public class AlreadyLoggedBaseTest extends BaseTests {
    @BeforeClass
    @Override
    public void SetUp() throws InterruptedException {
        super.SetUp();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        LoginPage loginPage = homePage.clickLoginLink();
        Thread.sleep(5000);
        loginPage.setUsername("kremena_victoria@yahoo.com");
        loginPage.setPassword("qwerty123");
        NewsfeedPage newsfeedPage = loginPage.clickLoginButton();
        Assert.assertEquals(newsfeedPage.getTitle(),"Funimation - Site SubPage", " Title does not match");
    }
}
