package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import util.Utils;

public class CreateRequestForOfferPage extends BasePage {

    public CreateRequestForOfferPage(WebDriver driver) {
        super(driver);
        By xpath = By.xpath("//*[@id=\"background-page\"]/div[3]/div");
        waitForElementToBeVisible(xpath, 10);
    }

    public void chooseRequestForKidsParty() {
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.radioBtnKidsParty"));
        waitForElementToBeClickable(xpath, 10);
        driver.findElement(xpath).click();
    }

    public void chooseRequestForBecomeComedian() {
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.radioBtnForComedian"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).click();
    }

    public void setPublicAge(String age) {
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.publicAgeField"));
        waitForElementToBeClickable(xpath, 10);
        driver.findElement(xpath).sendKeys(age);
    }

    public void setLocation(String city) {
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.cityLocation"));
        waitForElementToBeClickable(xpath, 10);
        driver.findElement(xpath).sendKeys(city);
    }

    public void setDataInCalendar(String data) {

    By xpath = By.xpath(Utils.getUIMappingByKey("xpath.calendarField"));
    waitForElementToBeVisible(xpath, 10);
    driver.findElement(xpath).sendKeys(data);
    }

    public void setTimeOfParty(String beginTime){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.timeOfPartyField"));
        waitForElementToBeClickable(xpath, 10);
        driver.findElement(xpath).sendKeys(beginTime);
    }

    public UserPostsListPage submitRequest(){
        By locator = By.xpath(Utils.getUIMappingByKey("xpath.requestSubmitButton"));
        if ( !driver.findElement(locator).isSelected() )
        {
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(locator)).click().perform();
        }
        return new UserPostsListPage(driver);
    }
}
