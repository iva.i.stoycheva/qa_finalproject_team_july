package pages.createRequestForOffer;

import base.AlreadyLoggedBaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CreateRequestForOfferPage;
import pages.UserPostsListPage;


public class CreateRequestBecomeAnimator extends AlreadyLoggedBaseTest {

        @Test (priority = 4)
        public void testSuccessfulRequestBecomeAnimator() throws InterruptedException{

            CreateRequestForOfferPage createRequestForofferPage = homePage.clickRequestButton();
            sleepForNMs(2000);
            createRequestForofferPage.chooseRequestForBecomeComedian();
            createRequestForofferPage.setPublicAge("11");
            createRequestForofferPage.setLocation("Sofia");
            createRequestForofferPage.setDataInCalendar("12312020");
            createRequestForofferPage.setTimeOfParty("12.00");

            UserPostsListPage userPostListPage = createRequestForofferPage.submitRequest();

            Assert.assertEquals(userPostListPage.getHeaderPostListText(), "Post's list", "Wrong indirection.");
            assertElementPresent("xpath.checkCreateRequest");
        }
    }
