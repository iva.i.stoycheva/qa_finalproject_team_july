TEST SUITS: Login Scenarios:
###GUI & Functionality:###

ID: 1 
Title: Login with valid credentials/username, password/
Description: As a user, I would like to be able to Login with valid credentials.
Prerequisites
Navigate to the url: `https://funimation1.herokuapp.com`
Steps: 
2. Click on the `Login button`
3. Enter the valid credentials/username and password/.
DATA: email:ivcheto1990@abv.bg, pass: 0987654321
Expected:User is Login successful and Home page is displayed.
Criteria: pass

ID: 2 
Title:Login with valid  ONLY username
Description: Verify is that user is able to Login only with username
Prerequisites
Navigate to the url: `https://funimation1.herokuapp.com`
Steps: 
2. Select the Login page
3. Enter the valid email 
DATA:  email: ivcheto1990@abv.bg
Expected: The user is not able to Login and it's displayed the message "invalid email or password"
Criteria: pass

ID: 3
Title:Login with invalid credentials
Description: Verify that User is NOT able to Login with invalid credentials.
Prerequisites
Navigate to the url: `https://funimation1.herokuapp.com`
Steps: 
2. Select the Login page and enter invalid credentials
DATA: email: ivcheto1232, password:0987654321
Expected: The user is NOT Login and it's displayed the message "invalid email and password"
Criteria: pass

ID:4
Title: Login Page - `Link Register`
Description: Verify the link `Register here` is active:
Steps:
1. Go to the Login Page
2. Select Register here
Expected: When the user click on the link `Register here`, the user should be able to redirect to Registration page
Criteria: pass

ID:5
Title:Login Page - Link Request 
Description: Verify the link Request is active:
Steps:
1. Go to the Login Page
2. Select Request here
Expected: When the user select the link `Request here`, the user should be able to redirect to Forgot Password
Criteria: pass


ID: 6
Title: Login button accessible for all users
Description: As a user, I would like Login button to be accessible
Prerequisite
Open application: https://funimation1.herokuapp.com/
Steps: 
2. Check if Login button is accessible for unregistered user 
Expected: Log in button is accessible
Criteria: pass 

ID: 7
Title: Login with UPPER case
Description: as a user, I would like to Login with UPPER credentials.
Prerequisites
Go to url: https://funimation1.herokuapp.com/
Steps: 
2. Select the Login Page
3. Enter the valid credentials with Uppercase
Data: IVCHETO1990@ABV.BG, 0987654321
Expected: The user is Login successful
Criteria: pass

###SECURITY###

ID:8
Title: Bullet signs - Data
Description: Verify that the data is visible as asterisk or bullet signs.
Steps:
1. Go to Login page 
2. Enter the password
Data: pass: 0987654321
Expected: The password is not visible /bullet signs/
Criteria: pass

ID: 9
Title: Verify that password is crypted.
Description: As a user,the password shoud not be visible
Steps: 
1. Navigate to the url: https://funimation1.herokuapp.com/
2. Enter the valid credentials
3. Copy the data from the password field & paste it into Notepad
Expected: The password should be crypted
Criteria: pass

ID: 10
Title: Registered user in new browser - redirect to Login page
Precondition: Registered user
Description: In the new browser Verify that Registered user is redirect to the Login page
Steps:
1. Login in the app.
2. Copy the url link and pasted in new browser.
DATA: Internet explorer
Expected:The user should be redirect to Login page
Criteria: pass

ID: 11
Title SQL injection attacks- Login page
Description: I would like to test is the website is protected from SQL injection.
Steps: 
1. Go to Login Page.
2. Enter the username and password
Data: email: xxx@xxx.xxx, pass: xxx') OR 1 = 1 -- ]
Expected: When entered the credentials, should be NOT able to Login.
Criteria: pass


TEST SUITS: LOG OUT
### LOG OUT ###

ID: 12
Title: Verify Logout Button is visible
Description: As a registered user, I would like to Logout
Steps: 
1. Go to url: https://funimation1.herokuapp.com/
2. Enter the valid credentials
3. On section My Account, select Logout button.
Expected: Logout button is easy to use
Criteria: pass

ID: 13
Title: Verify Logout button redirect to the Login Page
Description:As a user I would like the Logout button redirect to Login
Steps: 
1. Go to https://funimation1.herokuapp.com/
2. Find the Log out button
Expected: Log out button redirect to the Login 
Criteria: pass


TEST SUIT: FORGOT PASSWORD
###FORGOT PASSWORD & Functionality:###

ID: 14
Title:Verify forgot password button with valid credentials
Description: As a user, When I forgot password, I would like to send the email for resset the password
Precondition: try to Login multiple time unsuccessful.
Steps:
1. On section Forgot password
2. Enter the valid email address
2. Click on the Reset Password button
DATA: email: ivcheto1232@abv.bg
Expected: The page should show notification for sending an email.
Criteria: pass


ID: 15
Title:Forgot password button - invalid email
Description: As a user, When I forgot password, I would like to send the email for resset the password
Precondition: try to Login multiple time unsuccessful.
Steps:
1. On section Forgot password
2. Enter the invalid email address
2. Click on the Reset Password button
DATA: email: ivcheto1990@
Expected: The page should show notification for "must be a well-formed email address"
Criteria: pass

ID: 16
Title:Forgot password button - empty email
Description: Verify when user send empty email
Precondition: try to Login multiple time unsuccessful.
Steps:
1. On section Forgot password
2. Enter the empty email address
2. Click on the Reset Password button
Expected: The page should show notification for "must not be empty"
Criteria: pass

ID: 17
Title: Forgot password - Register button
Description: Verify the Register button is active
Precondition: Forgot password section
Steps
1. On the forgot password  page, select Register button
Expected result: The user should be able to redirect to Registration form
Criteria: pass


ID: 18
Title: Forgot password - Login button
Description: Verify the Login button is active
Precondition: Forgot password section
Steps
1. On the forgot password  page, select Login button
Expected result: The user should be able to redirect to Login form
Criteria: pass


###GUEST - UNREGISTERED USER###

ID: 19
Title: The NAV bar - Login, Registration, Profile search and Public feed
Description:
As a unregistered user, I would like to see the Application Home Page functionality
Steps: 
1. Open https://funimation1.herokuapp.com
Expected: The Home page should be visible without authentication
Criteria: pass 



ID: 
Title: Stand-Up comedian request 
Description:
As a unregistered user, I would like to see Stand-Up comedian request link
Steps: 
1. Open the application https://funimation1.herokuapp.com/
2. Check the link
Expected: The Stand-Up comedian request should be redirect to the All public request offers for stand-up comedians.
Criteria: pass 


ID: 
Title: Kids entertainment request 
Description:
As a unregistered user, I would like to see Kids entertainment request 
Steps: 
1. Open the application https://funimation1.herokuapp.com/
2. Check the link Kids entertainment request 
Expected: The Kids entertainment link should be redirect to the All request offers for stand-up comedians.
Criteria: pass 




ID: 20
Title: The NAV bar - Login form
Description:
As a unregistered user, I would like to check the Login form is accessible
Steps: 
1. Open https://funimation1.herokuapp.com
2. Select The Login form
Expected: The Login button should be redirect to Login Page
Criteria: pass 


ID: 21
Title: Login form - Forgotten password
Description:
As a unregistered user, I would like to check the Forgotten password in Login Page
Steps: 
1. Open https://funimation1.herokuapp.com
2. Select The Login form
3. Open the Forgot password link 
Expected: Forgot password redirect to the Forgot password page
Criteria: pass 


ID: 22
Title: Registration form
Description:
As a unregistered user, I would like to check the Registration form
Steps: 
1. Open https://funimation1.herokuapp.com
2. Select The Registration form
Expected: The unregistrered user, should be able to redirect to Registration form
Criteria: pass 


ID: 22
Title: Public feed
Description:
As a unregistered user, I would like to see Public feed
Steps: 
1. Open the application https://funimation1.herokuapp.com
2. Check for Public feed
Expected: The unregistrered user, should be able to see the ordered public posts
Criteria: pass 


ID: 22
Title: Unregistered User - Verify is the comment and like button in the Comment are permissible.
Description:
As a unregistered user, I would check the comment and like button in the Comment Section
Steps: 
1. Open the application https://funimation1.herokuapp.com
2. Check for Public feed
3. Select the Comment post/like button/
Expected: The unregistrered user, should NOT be able to Comment or Like posts
Criteria: pass  



### Search button ### 

ID: 23
Title: The Nav Bar - Search Button
Description:
As a unregistered user, I would like check result page title contains searched keyword
Steps: 
1. Open https://funimation1.herokuapp.com
2. Select the Search button
3. Enter in the Search fields
Data: "лампа"
Expected: The search button is active
Criteria: pass
 


ID: 23
Title: Search button - empty data
Description:
As a unregistered user, I would like Check the Search button - without entering any keyword
Steps: 
1. Open https://funimation1.herokuapp.com
2. Select the Search button
Expected: Should only Refresh the Home Page
Criteria: fail



ID: 24
Title: Search box keeps the history of previously searched keywords
Description:
As a unregistered user, I would like to check if search box keeps the history of previously searched keywords.
Steps: 
1. Open https://funimation1.herokuapp.com
2. Select the Search button
3. Enter tab with keyboard
Expected: The history of previously searched keywords should be visible
Criteria: pass


ID: 25
Title: Search fields - search results are displayed per page
Description:
As a unregistered user, I would like to check How many search results are displayed per page
Steps: 
1. Open https://funimation1.herokuapp.com
2. Select the Search button
3. Enter the keyword
Data: "Do"
Expected: The user should be able to receive  that results are displayed per page.
Criteria: pass

ID: 26
Title: Search fields - search results with the same keyword multiple time
Description:
As a unregistered user, I would like to check is that any discrepancy in result when searching with same keyword multiple times
Steps: 
1. Open https://funimation1.herokuapp.com
2. Select the Search button
3. Repeat the same data and enter the keyword
Data: "Do"
Expected: The user should be able to receive the same results searching with same keyword multiple times.
Criteria: pass

ID: 27
Title: Search fields - check the performance of rendering results for most frequently used search keywords
Description:
As a unregistered user, I would like to check is the performance of rendering results are good.
Steps: 
1. Open https://funimation1.herokuapp.com
2. Select the Search button
3. Enter in the Search field 
Data: "Do"
Expected: The user should be able to receive good performance.
Criteria: pass


ID: 28
Title: Search with the keyword that does not produce any result
Description:
I would like to check If I search with the keyword that not produce result.
Steps: 
1. Open https://funimation1.herokuapp.com
2. Select the Search button
3. Enter in the Search field 
Data: "време"
Expected: The user should be able to show up the message "No posts sorry"
Criteria: pass


###ADMIN ###
ID: 29
Title: Admin - User Part
Description:
As a Admin, I would like to check Basic Administrator, user basic CRUD operation.
Prerequisites
The user should have been login with Administration autorities.
Steps: 
1. Open the Admin panel
2. Check the button All users
Expected: The admin should see The userList
Criteria: pass

ID: 30
Title: Admin - All User, Check the UserList contains all basic CRUD functionality.
Description:
As a Admin, I would like to checked and use all the basic CRUD operation.
Prerequisites
The user should have been login with Administration autorities.
Steps: 
1. Open the Admin panel
2. Check the button All users
3. Verify that "Go to User Page, update, delete" buttons are visible.
Expected: The Go to User Page, update, delete are visible.
Criteria: pass


ID: 31
Title: Admin - Posts
Description:
As a Admin, I would like to see All posts
Prerequisites
The user should have been login with Administration autorities.
Steps: 
1. Open the Admin panel
2. Check the button Posts
Expected: The Admin should be able to see All Post
Criteria: pass

ID
PRIORITY
TITLE
DESCRIPTION
PRECONDITIONS
STEPS TO EXECUTE
EXPECTED RESULT
DATA NEEDED


32
1
Write a new post.

As a user I want to write a Post.

Login in the website:
 https://funimation.ml
1. Open https://funimation.ml
2. Login the website.
3. Click on the “POST” button
4. Write your post in the “Post body” field
5. Click on the “Title” field
6. Write a title of the current post
7. Click in the drop-down menu and choose the privacy: public or private
8. Click the button “create Post” 

 
4.Dfd
6.Fdd

8.The post is created and must be visible in the home page(the news page) and in the created post userpage.

Valid credentials


33
2
Delete a post.

As a user I want to delete my post.

Login in the website.
Write a post.

1. Open https://funimation.ml
2. Login the website.
3. Click on the “POST” button
4. Write your post in the “Post body” field
5. Click in the “title” field
6. Write a title of the current post
7. 7.Click in the drop-down menu and choose the privacy: public or private
8. 8.Click the button “create Post” 
9. Find your post in your personal page or in the feed in the homepage 
10. Find a button or drop-down menu with “delete” option
11. Click the button or the option of the drop-down menu
The existing post must be deleted.
Valid “username” and “password”,
Created post with title and body.


34
2
Update a post.

As a user I want to update my post content.

Login in the website.
Write a post.
1. Open https://funimation.ml
2. Login the website.
3. Click on the “POST” button
4. Write your post in the “Post body” field
5. Click in the “title” field
6. Write a title of the current post
7. 7.Click in the drop-down menu and choose the privacy: public or private
8. 8.Click the button “create Post” 
9. Find your post in your personal page or in the feed in the homepage 
10. Find a button or drop-down menu with “edit post” option
11. Click the option “edit post” of the drop-down menu
12. Edit the content of the post
13. Click the button “Save”

The post content is updated.

Valid username and password,
Created post with title and body.


35
3
Like or dislike a post

As a user I want to like or dislike a post

Login in the website.
Find a post.
1. Open https://funimation.ml
2. Login the website.
3. Find one post in the homepage
4. Find the button “Like” and see the small number inside the button, remember the number.
5. Click the button “Like”.
6. Prove that the number increase with 1.
7. Click again the “Like” button – dislike function
8. Prove that the number decrease with 1.

The small number inside the “Like” button increase and decrease with 1. Prove it.

Valid username and password
A created post.


36
2
Comment a post

As a user I want to comment my or other users post

Login in the website.
Find a post.
1. Open https://funimation.ml
2. Login the website.
3. Find one post in the homepage
4. Find the button “Comment” and click it
5. Click on comment field and write your comment
6. Click the button “add Comment”

The comment is created.

Valid username and password
An existing post.


37
2
Reply to a comment

As a user I want to reply of a post comment.

Login in the website.
Find a existing post with min one comment.
1. Open https://funimation.ml
2. Login the website.
3. Find one post in the homepage(id=”post-xx”)
4. Find the button “Comment” and click it
5. Click on comment field and write your comment
6. Click the button “add Comment
7. In the homepage find the same post with the writing comment
8. Click the “reply” button down the comment content
9. Write a replying in the reply field
10. Click the button “add Reply”

The reply is created.

Valid username, password
An existing post.



38
1
Create a request for offer  for kid's entertainer

As a user I want to send a request for offer for kid’s entertainer

Login in the website.
1. Open https://funimation.ml
2. Login the website.
3. Click on the “Request for offer” button
4. Check the checkbox “Request offer for kid's entertainer”
5. Click in average age field and write an average age of the kids
6. Click the location field and write the city
7. 7.Click the date for the party field and specify the date.
8. Click the specify time field and write the time of the beginning the party
9. 8.Click the button “create Post” 

The request for kid’s party is created and must be visible in the user page.

Valid credentioals


39
1
Create a request for stand up comedian

As a user I want to send a request to take a part in a kid party as a comedian

Login in the site.
1. Open https://funimation.ml
2. Login the website.
3. Click on the “Request for offer” button
4. Check the checkbox “Request offer for stand up comedian”
5. Click in average age field and write an average age of the kids
6. Click the location field and write the city
7. 7.Click the date for the party field and specify the date.
8. Click the specify time field and write the time of the beginning the party
9. 8.Click the button “create Post” 

The request to become a comedian is created and must be visible in the user page.

Valid credentials

Issues Report - https://drive.google.com/file/d/1gURyEQAOCTCYjc6-Qx4dbDEvYP1n-6hn/view?usp=sharing
