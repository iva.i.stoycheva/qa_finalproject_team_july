package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import util.Utils;

public class UserPostsListPage extends BasePage{

    public UserPostsListPage(WebDriver driver) {
        super(driver);
    }

    public String getHeaderPostListText() {
        By locator = By.xpath(Utils.getUIMappingByKey("xpath.postListHeader"));
        waitForElementToBeVisible(locator, 10);
        return driver.findElement(locator).getText();
    }
}
