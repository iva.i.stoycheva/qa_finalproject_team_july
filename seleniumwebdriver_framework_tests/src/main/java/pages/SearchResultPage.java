package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultPage {
    private WebDriver driver;

    SearchResultPage(WebDriver driver) {
        this.driver = driver;
    }
    public String getTitle(){
        return this.driver.getTitle();
    }
}
