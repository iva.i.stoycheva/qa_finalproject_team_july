package pages.myPage;

import base.AlreadyLoggedBaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.MyPage;
import pages.PendingRequestList;


public class PendingRequestListTest extends AlreadyLoggedBaseTest {
    @Test(priority = 2)
    public void checkPendingList() throws InterruptedException {
        sleepForNMs(3000);
        MyPage checkMyPage = homePage.clickMyPage();
        sleepForNMs(3000);
        PendingRequestList checkPendingRequestListTest = checkMyPage.clickPendingRequest();
        String pageMyTitle = checkPendingRequestListTest.getTitle();
        sleepForNMs(3000);
        Assert.assertEquals(pageMyTitle, "Friends list");
        assertElementPresent("xpath.PendingRequestListContainer");
    }
}
