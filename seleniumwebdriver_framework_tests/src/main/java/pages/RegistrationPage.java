package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.WebElement;
import util.Utils;

public class RegistrationPage extends BasePage{

    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    public void setFirstName(String firstName){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.firstNameField"));
        waitForElementToBeVisible(xpath,10);
        driver.findElement(xpath).sendKeys(firstName);
    }

    public void setLastName(String lastName){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.lastNameField"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).sendKeys(lastName);
    }

    public void setEmailAddress(String emailAddress){
        By xpath = By.xpath((Utils.getUIMappingByKey("xpath.emailField")));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).sendKeys(emailAddress);
    }

    public void confirmEmailAddress(String emailAddress){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.confirmEmailField"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).sendKeys(emailAddress);
    }

    public void setPassword(String pass){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.passwordField"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).sendKeys(pass);
    }

    public void confirmPassword(String pass){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.confirmPasswordField"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).sendKeys(pass);
    }

    public void selectAgreeTermsCheckbox(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.agreeTermsCheckbox"));
        if ( !driver.findElement(xpath).isSelected() )
        {
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(xpath)).click().perform();
        }
    }

    public SuccessfulRegistrationPage submitRegistrationForm(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.registerButton"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).click();
        return new SuccessfulRegistrationPage(driver);
    }
}
