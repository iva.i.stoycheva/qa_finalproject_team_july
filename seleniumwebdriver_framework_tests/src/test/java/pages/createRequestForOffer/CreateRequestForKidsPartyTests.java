package pages.createRequestForOffer;

import base.AlreadyLoggedBaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CreateRequestForOfferPage;
import pages.UserPostsListPage;

public class CreateRequestForKidsPartyTests extends AlreadyLoggedBaseTest {
    @Test (priority = 5)
    public void testSuccessfulRequestForKidsParty() {

        CreateRequestForOfferPage createRequestForofferPage = homePage.clickRequestButton();
        sleepForNMs(300);
        createRequestForofferPage.chooseRequestForKidsParty();
        createRequestForofferPage.setPublicAge("11");
        createRequestForofferPage.setLocation("Sofia");
        createRequestForofferPage.setDataInCalendar("12312020");
        createRequestForofferPage.setTimeOfParty("12.00");
        UserPostsListPage userPostListPage = createRequestForofferPage.submitRequest();
        Assert.assertEquals(userPostListPage.getHeaderPostListText(), "Post's list", "Wrong indirection.");
        assertElementPresent("xpath.checkRequestForOffer");

    }
}
