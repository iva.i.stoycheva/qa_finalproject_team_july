package pages.search;

import base.BaseTests;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.SearchResultPage;

public class SearchTest extends BaseTests {
    @Test (priority=1)
    public void testSearchTitle() throws InterruptedException {
        homePage.setSearchField("alaba"); // not existing post
        SearchResultPage searchResultPage = homePage.clickSearchButton();
        String searchResultPageTitle = searchResultPage.getTitle();
        By locator = By.xpath("//*[@id=\"background-page\"]/div[3]/div/div/div/div[1]/div/h2");
        waitForElementToBeVisible(locator, 10);
        Assert.assertEquals(searchResultPageTitle, "Funimation - Search results");
        Assert.assertEquals(driver.findElement(locator).getText(), "No posts, sorry", "Expected header does not match");
        assertElementPresent("xpath.CheckBody");
    }
}
