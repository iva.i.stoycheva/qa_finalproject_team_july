package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import util.Utils;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
        waitForElementToBeVisible(By.xpath("//*[@id=\"background-page\"]/div[3]/div/div/div/div/h1/b"), 10);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setUsername(String emailAddress) {
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.usernameField"));
        waitForElementToBeClickable(xpath, 10);
        driver.findElement(xpath).sendKeys(emailAddress);
    }

    public void setPassword(String password) {
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.passwordField"));
        waitForElementToBeClickable(xpath, 10);
        driver.findElement(xpath).sendKeys(password);
    }

    public NewsfeedPage clickLoginButton() {
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.loginButton"));
        waitForElementToBeClickable(xpath, 10);
        driver.findElement(xpath).click();
        return new NewsfeedPage(driver);
    }
}
