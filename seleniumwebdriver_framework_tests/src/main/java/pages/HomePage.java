package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.Utils;

import javax.rmi.CORBA.Util;


public class HomePage extends BasePage {

    public HomePage(WebDriver driver){
        super(driver);
    }

    public RegistrationPage clickRegistrationLink(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.registrationLink"));
        waitForElementToBeClickable(By.xpath(Utils.getUIMappingByKey("xpath.registrationForm")), 10);
        driver.findElement(xpath).click();
        return new RegistrationPage(driver);
    }

    public LoginPage clickLoginLink(){
        By locator = By.xpath(Utils.getUIMappingByKey("xpath.loginPageLink"));
        driver.findElement(locator).click();
        return new LoginPage(driver);
    }

    public void setSearchField(String searchField){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.searchInput"));
        driver.findElement(xpath).sendKeys(searchField);
    }

    public SearchResultPage clickSearchButton(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.searchButtonLnk"));
        driver.findElement(xpath).click();
        return new SearchResultPage(driver);
    }

    public StandUpComedianRequest clickOnTheStandUpRequest(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.clickOnTheStandUpRequest"));
        driver.findElement(xpath).click();
        return new StandUpComedianRequest(driver);
    }

    public KidsEntertainmentRequest clickTheKidsEntertainmentRequest(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.clickOnTheKidsEntertainmentRequest"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).click();
        return new KidsEntertainmentRequest(driver);
    }

    public MyPage clickMyPage(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.clickOnMyPage"));
        waitForElementToBeVisible(By.xpath(Utils.getUIMappingByKey("//*[@id=\"index-middle-column-1\"]/div")), 10);
        driver.findElement(xpath).click();
        return new MyPage(driver);
    }

    public  CreateRequestForOfferPage clickRequestButton(){
        By locator = By.xpath(Utils.getUIMappingByKey("xpath.requestForOfferButton"));
        waitForElementToBeClickable(locator, 10);
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(locator).click();
        return new CreateRequestForOfferPage(driver);
    }
}

