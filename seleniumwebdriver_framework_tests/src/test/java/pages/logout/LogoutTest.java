package pages.logout;

import base.AlreadyLoggedBaseTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LogoutTest extends AlreadyLoggedBaseTest {
    @Test
    public void testSucessfullLogout() throws InterruptedException{
        driver.findElement(By.xpath("//*[@id=\"background-page\"]/div[1]/div/div[3]/a")).click();

        driver.findElement(By.xpath("//*[@id=\"background-page\"]/div[1]/div/div[3]/div/a[4]")).click();

        Thread.sleep(5000);
        driver.findElement(By.xpath("html/body/div/div/input")).click();
        Thread.sleep(5000);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


        driver.findElement(By.xpath("/html/body/div/div/div[2]/div[1]/div/a[3]")).isDisplayed();
        Assert.assertTrue(driver.getPageSource().contains("Login"));
        Assert.assertTrue(driver.getPageSource().contains("Register"));

    }
}

