package pages.registration;

import base.BaseTests;
import fakeUser.User;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.RegistrationPage;
import pages.SuccessfulRegistrationPage;
import util.Utils;

public class RegistrationTests extends BaseTests {

    @Test (priority=1)
    public void testSuccessfulRegistration() throws InterruptedException {
        User fakeUser = new User();
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.registrationLink"));
        waitForElementToBeVisible(xpath, 10);
        sleepForNMs(3000);
        RegistrationPage registrationPage = homePage.clickRegistrationLink();
        sleepForNMs(3000);
        registrationPage.setFirstName(fakeUser.getFirstName());
        sleepForNMs(3000);
        registrationPage.setLastName(fakeUser.getLastName());
        sleepForNMs(3000);
        registrationPage.setEmailAddress(fakeUser.getEmail());
        registrationPage.confirmEmailAddress(fakeUser.getEmail());
        registrationPage.setPassword(fakeUser.getPassword());
        registrationPage.confirmPassword(fakeUser.getPassword());
        registrationPage.selectAgreeTermsCheckbox();
        sleepForNMs(3000);
        SuccessfulRegistrationPage successfulRegistrationPage = registrationPage.submitRegistrationForm();

        Assert.assertEquals(successfulRegistrationPage.getSuccessfulRegistrationText(),
                "You have successfully registered. A verification email has been sent to your e-mail address");
    }
}
