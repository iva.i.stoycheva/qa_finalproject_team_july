package testCases;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import util.UserActions;

public class BaseTest {
	UserActions actions = new UserActions();
	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser();
	}

	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}
}
