package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import util.Utils;

public class SuccessfulRegistrationPage {

    private WebDriver driver;
    private By registrationStatusAlert = By.xpath("//*[@id=\"background-page\"]/div[3]/div/div/div/div/div/span");

    public SuccessfulRegistrationPage(WebDriver driver){
        this.driver = driver;
    }

    public String getSuccessfulRegistrationText(){

        return driver.findElement(By.xpath(Utils.getUIMappingByKey("xpath.registrationStatusAlert"))).getText();
    }
}
