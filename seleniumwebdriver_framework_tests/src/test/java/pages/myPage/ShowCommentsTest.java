package pages.myPage;

import base.AlreadyLoggedBaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.MyPage;
import pages.MyPageShowComments;


public class ShowCommentsTest extends AlreadyLoggedBaseTest {
    @Test(priority = 2)
    public void checkShowCommentsTest() throws InterruptedException{
        sleepForNMs(5000);
        MyPage checkMyPage = homePage.clickMyPage();
        sleepForNMs(5000);
        MyPageShowComments showComments = checkMyPage.clickOnMyPageShowComments();
        sleepForNMs(5000);
        String commentTitle = showComments.getTitle();
        sleepForNMs(5000);
        Assert.assertEquals(commentTitle, "All Post Comments");
        assertElementPresent("xpath.showCommentsByPost");
    }
}
