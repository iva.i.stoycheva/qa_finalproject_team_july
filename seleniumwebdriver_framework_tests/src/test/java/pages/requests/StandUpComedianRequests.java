package pages.requests;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.StandUpComedianRequest;

public class StandUpComedianRequests extends BaseTests  {

    @Test(priority = 2)
    public void checkForComedianRequest()throws InterruptedException{
        StandUpComedianRequest checkForRequest = homePage.clickOnTheStandUpRequest();
        String checkForRequestTitle = checkForRequest.getTitle();
        Assert.assertEquals(checkForRequestTitle, "All request offers for stand-up comedians");
        assertElementPresent("xpath.StandUoComedyBody");
    }
}
