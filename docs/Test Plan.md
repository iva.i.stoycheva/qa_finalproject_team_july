  # TEST PLAN

  Link to GoogleDrive - https://drive.google.com/file/d/1XutMmCBTEaE97U4trSf5pU2Ylc4EvJkZ/view?usp=sharing
  
  | Date  | Version | Description           | Author       | Reviewer | Approver |
  |-------|---------|-----------------------|--------------|----------|----------|
  | 28.10 | 0.1     | Test plan was created | I. Stoycheva |          |          |
  | 12.02 | 0.2     | Test plan was created | M. Yordanov  |          |          |
  | 28.10 |         | Test plan was modify  | K. Stoyanova |          |          |
  |       |         |                       |              |          |          |
## Contents
    1. INTRODUCTION	
    1.1 Objectives	
    2.0 SCOPE	
    2.1 Features To Be Tested	
    2.2 Features Not To Be Tested	
    3.0 APPROACH	
    4.0 TESTING PROCESS	
    4.1 Test Deliverables	
    4.2 Responsibilities	
    4.3 Resources	
    4.4 Estimation and Schedule	
    5.0 ENVIRONMENT REQUIREMENTS	
    
    
    
### Introduction

#### 1.2 Objective
Customer wants a perfect website, which passed the full cycle of manual and automation testing. Given the specificity of the site it is very important to have the same quality and the site. The Test Plan has been created to facilitate communication within the team members. This document describes approaches and methodologies that will apply to the unit, integration and system testing of the `https://funimation1.herokuapp.com/`. It includes the scope, features to be tested and what not to be tested, testing process, approach, testing process identify the methods and criteria used in performing test activities, test deliverable, responsibilities and resources and estimation schedule.

#### 1.1.2 Primary Objectives
A primary objective of testing is to: assure that the system meets the full requirements, including quality requirements (functional and nonfunctional requirements) and fit metrics for each quality requirement and satisfies the use case scenarios and maintain the quality of the product.

#### 1.1.3 Secondary Objectives
The secondary objectives of testing will be to: identify and expose all issues and associated risks, communicate all known issues to the project team, and ensure that all issues are addressed in an appropriate matter before release.

### Scope
The document mainly targets the GUI testing and validating data in report output as per Requirements Specifications provided by Client.

#### 2.1 Features to Be Tested

* Login form
* Registration form
* Profile search
* Create post and comment
* Create request for offer
* Public feed
* Administration part

#### 2.2 Features Not to Be Tested
Not other than mentioned above in section 2.1

#### 3 Approach
The approach, that used, is Analytical therefore, in accordance to requirements-based strategy, where an analysis of the requirements specification forms the basis for planning, Test Plan estimating and designing tests. Test cases will be created during exploratory testing. All test types are determined in Test Process. The project is using an agile approach, with weekly iterations. At the end of each week the requirements identified for that iteration will be delivered to the team and will be tested.

### 4 Testing Process

#### 4.1 Entry Criteria 
- All the necessary documentation, design, and requirements information should be
available that will allow testers to operate the system and judge the correct behavior.
- All the standard software tools including the testing tools must have been
successfully installed and functioning properly.
- The test environment such as, lab, hardware, software, and system administration support should be ready.
- QA resources have completely understood the requirements.

#### 4.2 Exit criteria
- 100% of Critical test cases pass
- 80 % of High test cases pass
- All regression tests pass

#### 4.3 Testing Types
* Black box Testing: It is some behavioral testing or Partitioning testing. This kind of testing focuses on the time functional requirements of the software. It enable one to derive sets of input conditions that will fully exercise all functional requirements for web application.

* Exploratory testing is all about discovery, investigation, and learning. It emphasizes personal freedom and responsibility of the individual tester. It is defined as a type of testing where Test cases are not created in advance but testers check system on the fly.

* GUI Testing: GUI testing will includes testing the UI part of report. It covers users Report format, look and feel, error messages, spelling mistakes, GUI guideline violations.

* Integration Testing: used to test individual software components or units of code to verify interaction between various software components and detect interface defects. Components are tested as a single group or organized in an iterative manner. After the integration testing has been performed on the components, they are readily available for system testing.

* Functional Testing: Functional testing is carried out in order to find out unexpected behavior of the report. The characteristic of functional testing are to provide correctness, reliability, testability and accuracy of the report output/data.

* System Testing: System testing of software is testing conducted on a complete, integrated system to evaluate the system's compliance with its specified requirements.

* Usability testing -  Testing to determine the extent to which the software product is understood, easy to learn, easy to operate and attractive to the users under specified conditions.

#### 4.4 Bug Severity and Priority fields

| Severity ID | Severity | Severity Description                                                                         |
|-------------|----------|----------------------------------------------------------------------------------------------|
| 1           | Critical | Showstopper, major failure that affects the entire system or multiple modules of the system. |
| 2           | High     | This is big, a major piece of functionality of the application is broken                     |
| 3           | Medium   | The feature is not working correctly, but it will not impact users greatly                   |
| 4           | Minor    | Could live with the issue, should be fixed when possible                                     |


| Priority  ID | Priority  | Priority Description |
|--------------|-----------|----------------------|
| 1            | Priority1 | Must Fix             |
| 2            | Priority2 | Should Fix           |
| 3            | Priority3 | Fix When Have Time   |
| 4            | Priority4 | Low Priority         |


#### 4.5  Test Deliverables
1. Test Plan
2. High Level Test Cases
3. API Tests
4. UI tests
5. Test Report
6. Bug report

#### 4.6 Responsibilities

QA role in test process
- Preparing Test Cases:
	- Reviewing test cases.
	- Creating Test Data.
- Executing Test Cases: manual and automation
- Retesting and Regression Testing: 

### 5 Resources

#### 5.1 Testing Tool

| Process              | Testing Tool                                   |
|----------------------|------------------------------------------------|
| Create test case     | TestRail                                       |
| Test case tracking   | TestRail                                       |
| Test case execution  | Manual, Selenium, Postman, Test NG, Jbehave    |
| Test case management | Trello                                         |
| Defect management    | Gitlab                                         |
| Test reporting       | TestRail                                       |

#### 5.2 Configuration management

* Documents: Trello, TestRail
* Code: Gitlab

#### 4.8 Estimation and Schedule

| Task name                                          | Start | Finish | Effort | Comments |
|----------------------------------------------------|-------|--------|--------|----------|
| Test Planning                                      |28.10  |29.10   |        |          |
| Review Requirements documents                      |29.10  |        |        |          |
| Create high level test cases                       |02.11  |25.11   |        |          |
| Start and train new test ressources                |05.11  |        |        |          |
| Exploratory testing - manual check for happy path  |10.11  |22.10   |        |          |
| Automation tests - happy paths / Selenium, TestNG/ |23.11  |1.12    |        |          |
| Jbehave for BDD scenarios                          |30.11  |01.12   |        |          |
| API testing - Postman                              |16.11  |29.11   |        |          |
| Report - Automation tests                          |03.12  |03.12   |        |          |
| Create a coverage report                           |02.12  |02.12   |        |          |
| Create bat/script file for automation tests        |01.12  |01.12   |        |          |

### 5 ENVIRONMENT REQUIREMENTS
Test Team  - 3 QA testers

* Support level 1 (browsers);
* Windows 10: Chrome(Version 78.0.3904.70 (Official Build) (64-bit)), 
* Firefox(69.0.3 (64-bit)), 


* Support level 3 (devices)
* Windows – Lenovo Ideapad 330 s, SSD, 1920 x 1080, 14"
* Kremi - 1. Asus ASUS ZenBook 14 Ultra-Slim Laptop 14” 8th-Gen Intel Core i7-8565U Processor, 
  512GB PCIe SSD, Windows 10resolution 1920x1080 chrome an mozilla and Microsoft Edge
  2.Lenovo IdeaPad L340-15IRH,Intel® Core™ i7-9750H 2.60 GHz ,GeForce GTX 1050 (3GB GDDR5), Windows 10,
  Browsers; Chrome, Mozilla, Microsoft Edge, Opera - last versions only
* Marto - Dell Inspiron N5110, Intel i3-2110M, 8GB Memory, 500 HDD, Intel HD Graphics 3000,  1366x768, 15.6"

#### 5.1 TERMS/ACRONYMS 
* The below terms are used as examples, please add/remove any terms relevant to the document. 

| TERM/ACRONYM | DEFINITION                    |
|--------------|-------------------------------|
| API          | Application Program Interface |
| GUI          | Graphical user interface      |
| UAT          | User acceptance testing       |
| QA           | Quality Assurance             |













