package pages.createPost;

import base.AlreadyLoggedBaseTest;
import com.github.javafaker.Faker;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CreatePostPage;
import pages.NewsfeedPage;
import pages.UserPostsListPage;

public class CreatePostTests extends AlreadyLoggedBaseTest {

    @Test (priority=3)
    public void testSuccessfulCreatedPost() throws InterruptedException {
        Faker faker = new Faker();
        NewsfeedPage newsfeedPage = new NewsfeedPage(driver);
        Assert.assertEquals(driver.getTitle(),"Funimation - Site SubPage", "News feed tittle does not match the expected state");

        Thread.sleep(1000);
        CreatePostPage createPostPage = newsfeedPage.clickCreatePostButton();
        Thread.sleep(1000);
        Assert.assertEquals(driver.getTitle(),"Create post", "Create post tittle does not match the expected state");

        createPostPage.setPostTitle(faker.rickAndMorty().character() + " "+ faker.rickAndMorty().location());
        createPostPage.createPostContent(faker.rickAndMorty().quote());
        UserPostsListPage userPostListPage = createPostPage.submitPost();
        Thread.sleep(5000);
        Assert.assertEquals(userPostListPage.getHeaderPostListText(), "Post's list", "Error here");
        assertElementPresent("xpath.assertCreatedPost");
    }
}
