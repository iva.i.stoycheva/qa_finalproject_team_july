package pages.requests;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.KidsEntertainmentRequest;

public class KidsEntertainmentRequestTest extends BaseTests {

    @Test(priority = 2)
    public void checkForKidsEntertainmentRequest() throws InterruptedException{
        KidsEntertainmentRequest checkForKidsRequest = homePage.clickTheKidsEntertainmentRequest();
        String checkForRequestTitle = checkForKidsRequest.getTitle();
        Assert.assertEquals(checkForRequestTitle, "All request offers for kid's entertainment");
        assertElementPresent("xpath.KidsEntertainmentRequestBody");
    }
}
