package stepdefinitions;

import org.jbehave.core.annotations.*;
import util.Utils;

public class BaseStepDefinitions {
    @BeforeStory
    public void beforeStory(){
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
    }

    @AfterStory
    public void afterStory(){
        Utils.getWebDriver().manage().deleteAllCookies();
    }

    @AfterStories
    public void afterStories(){
        Utils.getWebDriver().quit();
    }



}
