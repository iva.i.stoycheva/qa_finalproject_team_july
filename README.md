### INTRO 
 
 This is sample repository for testing the [Web Аpplication](https://funimation1.herokuapp.com)
 
    - Acces the application locally (localhost:8080) 
    - Open GitBash
    - git clone https://gitlab.com/traykovvl/Funimation/tree/QA_master_branch
    - cd to Funimation/socnet/
    - gradle bootRun

* Link to Trello [Board](https://trello.com/b/3WpHnL6T/backlog-qa-team)

* Link to TestTrail [Here](https://howtoqa.testrail.io/index.php?/reports/view/92)



### API tests with Postman
We test the following site: https://funimation1.herokuapp.com/
Here are our API service tests, made in Postman.

With a registration in Postman or with your gmail account, you can see and explore the tests collection in the following link:
https://documenter.getpostman.com/view/8263656/SW7gTjro?version=latest

The environment which the collection need is in baseUrl_environment.json.

********************************************************************************
### FUNIMATION MOCK SERVER
We made a Mock Server for the same website: https://funimation1.herokuapp.com/
Each query has several examples for the different response from the server.
Each query is tested with several tests, tested are the headers, the status, the jason content in the responses.
Here is the documentation: https://documenter.getpostman.com/view/8263656/SW7gV5wS?version=latest

********************************************************************************
###  How to run Postman with Newman
Newman is a command line Collection Runner for Postman. It allows you to run and test a Postman Collection directly from the command line.
Newman is built on Node.js. To run Newman, make sure you have Node.js installed.
    
    
    - npm install -g newman
    - Open Git Bash
    - git clone https://gitlab.com/iva.i.stoycheva/qa_finalproject_team_july.git
    - newman run Funianimation_part_1.postman_collection.json -e baseUrl.json
    - newman run Funianimation_part_2.postman_collection.json -e baseUrl.json
    
********************************************************************************
### CI/CD in Gitlab using Newman
In the project we demonstrate how we can work using CI/CD Gitlab functionallity.
For this we use the collection: Testing.postman_collection.json and run the CI with gitlab-ci.yml file.
Next we can go in CI/CD -> Pipeline and click Run Pipline.
The result we can find in CI/CD -> Jobs and can download as html file: report.html !!!

********************************************************************************
### UI automation testing with Selenium
For our GUI tests we have:

1. Java programming language: Java JDK 8
2. InteliJ IDEA ultimate editor
3. Chrome driver, Gecko driver exe-s
4. Chrome browser, Firefox browser
5. We use Selenium Webdriver - an object-oriented automation API that nativly drives a browser as an user would.

We use Maven for out framework. In our POM file we add all dependencies for Java, Selenium-driver, TestNG, JBehave, JUnit.
For our Selenium prject we have two layers: framework lawer and test lawer.
Our framework is in src/main/java. Here are all of the iteractions with the web browser and all of the calls in the aplication.
Out tests are in scr/test/java. There we focus to the tests itself.

We used a pattern - page object model design pattern.
This mean every page of the application is repressenting from a java class in package "pages" in out framwork in src/main.

And all of our test cases are in the src/test folder. As example there we create a pachake "login" and in this pachake we create a java class "LoginTests". In this class we put all of the tests connected with login functionallity we put in this class, for each test - different methods in the class "LoginTests".
In our tests in pachake "base" in class "BaseTests" we set up out driver with a method with annotation @BeforeClass and quit it with a method with annotation @AfterClass. All of our tests extend this class and this will run before each of our test classes.
...............................................................
    