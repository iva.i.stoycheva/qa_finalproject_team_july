package pages.myPage;

import base.AlreadyLoggedBaseTest;
import org.testng.annotations.Test;
import pages.FriendList;
import pages.MyPage;


public class FriendListTest extends AlreadyLoggedBaseTest {

    @Test(priority = 2)
    public void CheckFriendList() throws InterruptedException {
        sleepForNMs(3000);
        MyPage checkMyPage = homePage.clickMyPage();
        sleepForNMs(3000);
        FriendList checkFriendListTest = checkMyPage.clickOnTheFriendList();

        assertElementPresent("xpath.FriendListMyItems");
        assertElementPresent("xpath.FriendListAssertByName");

    }

}
