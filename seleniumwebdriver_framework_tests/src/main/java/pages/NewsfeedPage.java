package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.Utils;

import java.util.concurrent.TimeUnit;

public class NewsfeedPage extends BasePage{

    public NewsfeedPage(WebDriver driver) {
        super(driver);
        // wait until the pages is visible
        By locator = By.xpath("//*[@id=\"index-middle-column-1\"]/div[1]/div/div/div/h2/b/span");
        waitForElementToBeVisible(locator, 10);
    }

    public NewsfeedPage newsfeedPage(){
        WebElement element1 = driver.findElement(By.xpath(Utils.getUIMappingByKey("xpath.postButton")));
        WebDriverWait wait = new WebDriverWait(driver, 10);
        return new NewsfeedPage(driver);
    }

    public String getAlertText(){
        return driver.findElement(By.xpath(Utils.getUIMappingByKey("xpath.statusAlert"))).getText();
    }

    public CreatePostPage clickCreatePostButton(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.postButton"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).click();
        return new CreatePostPage(driver);
    }
    public String getTitle(){
        return driver.getTitle();
    }
}
