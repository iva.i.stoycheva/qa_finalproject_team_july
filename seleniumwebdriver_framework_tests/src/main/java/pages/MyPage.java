package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import util.Utils;

public class MyPage extends BasePage{

    public MyPage(WebDriver driver) {
        super(driver);
    }

    public  String getTitle() { return this.driver.getTitle();
    }

    public FriendList clickOnTheFriendList(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.friendList"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).click();
        return new FriendList(driver);
    }
    public MyPageShowComments clickOnMyPageShowComments(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.myPageShowComments"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).click();
        return new MyPageShowComments(driver);
    }
    public PendingRequestList clickPendingRequest(){
        By xpath = By.xpath(Utils.getUIMappingByKey("xpath.pendingRequestLIst"));
        waitForElementToBeVisible(xpath, 10);
        driver.findElement(xpath).click();
        return new PendingRequestList(driver);
    }
}


