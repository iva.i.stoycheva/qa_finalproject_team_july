package fakeUser;

import com.github.javafaker.Faker;

public class User {
    String firstName;
    String lastName;
    String email;
    String password;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public User() {
        Faker faker = new Faker();

        this.firstName = faker.name().firstName();
        this.lastName = faker.name().lastName();
        this.email = this.firstName+this.lastName + faker.number().randomDigit() +"@mailinator.com";
        this.password = "RandomPass12345";
    }
}
