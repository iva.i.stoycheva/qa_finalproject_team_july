##!/usr/bin/env bash

git clone https://gitlab.com/iva.i.stoycheva/qa_finalproject_team_july.git 
cd qa_finalproject_team_july || exit 1
cd seleniumwebdriver_framework_tests || exit 2

mvn test 

## to run test command open file downlaod_and_run.sh;
