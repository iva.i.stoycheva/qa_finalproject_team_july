package pages.login;

import base.BaseTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.NewsfeedPage;

public class LoginTests extends BaseTests {

    @Test (priority=1)
    public void testSuccessfulLogin() throws InterruptedException {
        LoginPage loginPage = homePage.clickLoginLink();
        sleepForNMs(1000);
        loginPage.setUsername("kremena_victoria@yahoo.com");
        loginPage.setPassword("qwerty123");
        NewsfeedPage newsfeedPage = loginPage.clickLoginButton();
        Assert.assertEquals(newsfeedPage.getTitle(),"Funimation - Site SubPage", " Title does not match");
        assertElementPresent("xpath.clickOnMyPage");
    }
}
